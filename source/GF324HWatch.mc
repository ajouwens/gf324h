using Toybox.Application as App;

class GF324HWatch extends App.AppBase {
    function onStart() {}

    function onStop() {}

    function getInitialView() {
        return [new GF324H()];
    }
}
