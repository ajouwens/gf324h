using Toybox.Graphics as Gfx;
using Toybox.System as Sys;
using Toybox.Lang as Lang;
using Toybox.Math as Math;
using Toybox.Time as Time;
using Toybox.Time.Gregorian as Calendar;
using Toybox.WatchUi as Ui;

class GF324H extends Ui.WatchFace {
    var pi12 = Math.PI / 12.0;
    var pi30 = Math.PI / 30.0;
    var pi50 = Math.PI / 50.0;
    var pi6  = Math.PI / 6.0;
    var pi7  = Math.PI / 3.5;
    var cx;
    var cy;
    var width;
    var height;
    var active = false;
    var outerHourHand = new [7];
    var innerHourHand = new [5];
    var outerMinuteHand = new [7];
    var innerMinuteHand = new [5];
    var orangeSmallHand = new [5];
    var bckgrnd;

    //! Constructor
    function initialize() {
        bckgrnd = Ui.loadResource(Rez.Drawables.bckgrnd);
        outerHourHand = [ [-3,-6], [-5,-12], [-5,-70], [0,-80], [5,-70], [5,-12], [3,-6] ];
        innerHourHand = [ [-2,-15], [-2,-58], [0,-68], [2,-58], [2,-15] ];
        outerMinuteHand = [ [-3,-6], [-5,-12], [-5,-88], [0,-98], [5,-88], [5,-12], [3,-6] ];
        innerMinuteHand = [ [-2,-15], [-2,-78], [0,-88], [2,-78], [2,-15] ];
        orangeSmallHand = [ [-2,0], [-2,-15], [0,-19], [2,-15], [2,0] ];
    }

    function onLayout(dc) {
        width = dc.getWidth();
        height = dc.getHeight();
        cx = width / 2;
        cy = height / 2;
    }

    function onShow() {}

    function onHide() {}

    function onExitSleep() {
        active = true;
    }

    function onEnterSleep() {
        active = false;
    }

    function onUpdate(dc) {
        dc.setColor(Gfx.COLOR_WHITE, Gfx.COLOR_BLACK, Gfx.COLOR_WHITE);
        dc.clear();
        dc.drawBitmap(0, 0, bckgrnd);
        var hour= Sys.getClockTime().hour;
        var min = Sys.getClockTime().min;
        var now = Time.now();
        var info = Calendar.info(now, Time.FORMAT_SHORT);
        var batt = (Sys.getSystemStats().battery).toDouble();


        dc.setColor(Gfx.COLOR_ORANGE, Gfx.COLOR_TRANSPARENT);
        dc.drawText(cx+51, cy+39, Gfx.FONT_TINY, Lang.format("$1$", [info.day]), Gfx.TEXT_JUSTIFY_CENTER);
        dc.fillCircle(cx-42, cy, 4);
        dc.fillCircle(cx, cy+47, 4);
        dc.fillCircle(cx+46, cy, 4);

        dc.setColor(Gfx.COLOR_DK_GRAY, Gfx.COLOR_TRANSPARENT);
        dc.fillCircle(cx, cy, 8);

        dc.setColor(Gfx.COLOR_BLACK, Gfx.COLOR_TRANSPARENT);
        dc.fillCircle(cx, cy, 4);

        // Draw the day hand
        var day = (info.day_of_week - 1) * pi7;
        drawHand(dc, day, cx+46, cy, orangeSmallHand, Gfx.COLOR_ORANGE);
        // Draw the month hand
        drawHand(dc, (info.month - 1) * pi6, cx, cy+47, orangeSmallHand, Gfx.COLOR_ORANGE);
        // Draw the battery hand
        drawHand(dc, batt*pi50, cx-42, cy, orangeSmallHand, Gfx.COLOR_ORANGE);

        // Black dot on the orange day / month / battery hands
        dc.setColor(Gfx.COLOR_BLACK, Gfx.COLOR_TRANSPARENT);
        dc.fillCircle(cx-42, cy, 2);
        dc.fillCircle(cx, cy+47, 2);
        dc.fillCircle(cx+46, cy, 2);

        // Draw the hour hand
        hour = hour + (min / 60.0);
        hour = hour * pi12;
        drawHand(dc, hour, cx, cy, outerHourHand, Gfx.COLOR_DK_GRAY);
        drawHand(dc, hour, cx, cy, innerHourHand, Gfx.COLOR_WHITE);

        // Draw the minute hand
        min = min * pi30;
        drawHand(dc, min, cx, cy, outerMinuteHand, Gfx.COLOR_DK_GRAY);
        drawHand(dc, min, cx, cy, innerMinuteHand, Gfx.COLOR_WHITE);
    }

    function drawHand(dc, angle, centerX, centerY, coords, color) {
        var result = new [coords.size()];
        var cos = Math.cos(angle);
        var sin = Math.sin(angle);

        // Transform the coordinates
        for (var i = 0; i < coords.size(); i += 1) {
            var x = (coords[i][0] * cos) - (coords[i][1] * sin) + centerX;
            var y = (coords[i][0] * sin) + (coords[i][1] * cos) + centerY;
            result[i] = [x, y];
        }
        dc.setColor(color, Gfx.COLOR_TRANSPARENT, color);
        dc.fillPolygon(result);
    }
}
