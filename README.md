GF324H - Garmin fenix 3 - 24 hour watchface
===============================================

Description
-----------
This watch face shows the time on a 24 hour scale instead of the common 12 hour scale. 

Version history
---------------
###V1.2:
+ support for D2 Bravo
+ minor fixes (style etc.)
###V1.1:
+ Increased font size slightly for better readability.
###V1.0:
+ 24 hour clock marks
+ 60 minute clock marks
+ shows day, day of the week and month (as number)
+ battery indicator

Garmin forum
------------
